using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV3_AP
{
    class Dataset:IPrototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }
        public IPrototype Clone()
        {
            Dataset deepCopyDataset = (Dataset)this.MemberwiseClone();
            deepCopyDataset.data = new List<List<string>>();

            for (int i = 0; i < this.data.Count; i++)
            {
                deepCopyDataset.data.Add(new List<string>());
                for (int j = 0; j < this.data[i].Count; j++)
                {
                    deepCopyDataset.data[i].Add(this.data[i][j]);
                }
            }
            return deepCopyDataset;
        }


    }
}

//potebno je napraviti duboko kopiranje jer radimo s listama