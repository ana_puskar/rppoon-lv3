using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV3_AP
{
    interface IPrototype
    {
        IPrototype Clone();
    }
}