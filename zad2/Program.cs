using System;

namespace RPPOON_LV3_AP_zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            double[][] matrix = MatrixGenerator.GetInstance().GenerateMatrix(8, 4);
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write(matrix[i][j].ToString() + " ");
                }
                Console.WriteLine();
            }
        }
    }
}