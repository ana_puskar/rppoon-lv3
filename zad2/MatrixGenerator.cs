using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV3_AP_zad2
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private Random generator;
        

        
        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                
                instance = new MatrixGenerator();
            }
            return instance;
        }

        public double[][] GenerateMatrix(int rows, int columns)
        {
            double[][] matrix = new double[rows][];
            Random randomNumber = new Random();
            for (int i = 0; i < rows; i++)
            {
                matrix[i] = new double[columns];

                for (int j = 0; j < columns; j++)
                {
                    matrix[i][j] = randomNumber.NextDouble();
                }
            }

            return matrix;
        }

    }

   
}

//metoda GenerateMatrix(int, int) ima dvije odgovornosti: alokacija memorije i popunjavanje matrice nasumicnim brojevima
